import 'package:flutter/material.dart';
import 'package:practicewithsqlite/database_helper.dart';
import 'package:practicewithsqlite/model.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var nameController = TextEditingController();
  int? selectedId;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: TextFormField(
          controller: nameController,
        ),
      ),
      body: FutureBuilder<List<Note>>(
          future: DatabaseHelper.instance.getNote(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return ListView(
                children: snapshot.data!.map((i) {
              return Card(
                child: ListTile(
                  tileColor: selectedId==i.id?Colors.blueGrey:Colors.grey,
                  onTap: () {
                    setState(() {
                      nameController.text = i.name!;
                      selectedId = i.id;
                    });
                  },
                  onLongPress: () {
                    setState(() {
                      DatabaseHelper.instance.delete(i.id!);
                    });
                  },
                  title: Text(i.name!),
                ),
              );
            }).toList());
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          selectedId != null
              ? await DatabaseHelper.instance
                  .update(Note(id: selectedId, name: nameController.text))
              : await DatabaseHelper.instance
                  .add(Note(name: nameController.text));
          setState(() {
            nameController.clear();
            selectedId = null;
          });
        },
        child: Text("Add"),
      ),
    );
  }
}
