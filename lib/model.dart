class Note {
  final int? id;
  final String? name;

  Note({this.id, this.name});

  factory Note.fromMap(Map<String, dynamic> json) {
    return Note(id: json['id'], name: json['name']);
  }

  Map<String, dynamic> toMap() {
    return {'id': id, 'name': name};
  }
}
