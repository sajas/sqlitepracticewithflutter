import 'package:practicewithsqlite/model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  DatabaseHelper._privateContructor();
  static final DatabaseHelper instance = DatabaseHelper._privateContructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, 'note.dart');
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db
        .execute('''CREATE TABLE note(id INTEGER PRIMARY KEY, name TEXT)''');
  }

  Future<List<Note>> getNote() async {
    Database db = await instance.database;
    var notes = await db.query('note', orderBy: 'id');
    List<Note> noteList =
        notes.isNotEmpty ? notes.map((c) => Note.fromMap(c)).toList() : [];

    return noteList;
  }

  Future<int> add(Note note) async {
    Database db = await instance.database;
    return await db.insert('note', note.toMap());
  }

  Future<int> delete(int id) async {
    Database db = await instance.database;
    return await db.delete('note', where: 'id=?', whereArgs: [id]);
  }

  Future<int> update(Note note) async {
    Database db = await instance.database;

    return await db
        .update('note', note.toMap(), where: 'id=?', whereArgs: [note.id]);
  }
}
